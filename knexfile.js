// Update with your config settings.
require('dotenv').config()

module.exports = {
  development: {
    client: 'pg',
    connection: {
      host: process.env.POSTGRES_HOST_DEV,
      database: process.env.POSTGRES_NAME_DEV,
      user: process.env.POSTGRES_USER_DEV,
      password: process.env.POSTGRES_PASSWORD_DEV,
    },
    migrations: {
      directory: './src/database/migrations',
      tableName: 'migrations'
    },
    seeds: {
      directory: './src/database/seeds',
    }
  },

  staging: {
    client: 'pg',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  },

  production: {
    client: 'pg',
    connection: {
      database: 'my_db',
      user:     'username',
      password: 'password'
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'migrations'
    }
  }

};
