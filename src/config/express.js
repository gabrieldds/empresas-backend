const bodyParser = require('body-parser');
const cors       = require('cors');
const routes     = require('../routes');
const express    = require('express');
require('dotenv').config();

const app = express();
app.set('port', process.env.PORT || 3000);

app.use(bodyParser.json());
app.use(cors());
app.use(
    bodyParser.urlencoded({
      extended: true,
    }),
);

app.use('/api/v1', routes);

module.exports = app;