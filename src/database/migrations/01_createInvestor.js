const models = require('../models');

exports.up = (knex) => {
    return knex.schema.createTable('investors', models.investor.investorModel);
}

exports.down = (knex) => {
    return knex.schema.dropTable('investors');
}