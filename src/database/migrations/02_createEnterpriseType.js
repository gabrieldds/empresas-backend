const models = require('../models');

exports.up = (knex) => {
    return knex.schema.createTable('enterprise_types', models.enterpriseType.enterpriseTypeModel);
}

exports.down = (knex) => {
    return knex.schema.dropTable('enterprise_types');
}