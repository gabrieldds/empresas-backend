const models = require('../models');

exports.up = (knex) => {
    return knex.schema.createTable('enterprises', models.enterprise.enterpriseModel);
}

exports.down = (knex) => {
    return knex.schema.dropTable('enterprises');
}