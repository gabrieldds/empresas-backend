const models = require('../models');

exports.up = (knex) => {
    return knex.schema.createTable('investors_enterprises', models.investorsEnterprises.investorsEnterprisesModel);
}

exports.down = (knex) => {
    return knex.schema.dropTable('investors_enterprises');
}