module.exports = {
    enterpriseModel: (table) => {
        table.increments('id').primary();
        table.string('email_enterprise');
        table.string('facebook');
        table.string('twitter');
        table.string('linkedin');
        table.string('phone');
        table.string('own_enterprise');
        table.string('enterprise_name').notNullable();
        table.string('photo', 1000);
        table.string('description', 1000).notNullable();
        table.string('city').notNullable();
        table.string('country').notNullable();
        table.integer('value').notNullable();
        table.integer('shares').notNullable();
        table.integer('own_shares').notNullable();
        table.decimal('share_price').notNullable();
        table.integer('type_id').notNullable();

        table.foreign('type_id').references('id').inTable('enterprise_types');
    }
}