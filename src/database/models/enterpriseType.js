module.exports = {
    enterpriseTypeModel: (table) => {
        table.increments('id').primary();
        table.string('enterprise_type_name').notNullable();
    }
}