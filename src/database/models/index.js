const investor             = require('./investor');
const enterprise           = require('./enterprise');
const enterpriseType       = require('./enterpriseType');
const investorsEnterprises = require('./investorsEnterprises');

module.exports = {
    investor,
    enterprise,
    enterpriseType,
    investorsEnterprises,
};