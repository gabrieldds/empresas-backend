module.exports = {
    investorModel: (table) => {
        table.increments('id');
        table.string('investor_name').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('city').notNullable();
        table.string('country').notNullable();
        table.decimal('balance');
        table.string('photo');
        table.decimal('portfolio_value');
        table.boolean('first_access');
        table.boolean('super_angel');
        table.string("enterprise");
    }
}