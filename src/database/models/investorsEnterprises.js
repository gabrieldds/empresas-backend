module.exports = {
    investorsEnterprisesModel: (table) => {
        table.integer('investor_id').notNullable();
        table.integer('enterprise_id').notNullable();

        table.foreign('investor_id').references('id').inTable('investors');
        table.foreign('enterprise_id').references('id').inTable('enterprises');
    }
}