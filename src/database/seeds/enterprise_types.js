
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('enterprise_types').del()
    .then(function () {
      // Inserts seed entries
      return knex('enterprise_types').insert([
        {enterprise_type_name: 'Agro'},
        {enterprise_type_name: 'Aviation'},
        {enterprise_type_name: 'Biotech'}
      ]);
    });
};
