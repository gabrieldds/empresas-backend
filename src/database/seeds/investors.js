
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('investors').del()
    .then(function () {
      // Inserts seed entries
      return knex('investors').insert([
        {
          investor_name: 'Teste Apple',
          password: '12341234',
          email: 'testeapple@ioasys.com.br',
          city: 'BH',
          country: 'Brasil',
          balance: 350000.0,
          photo: '/uploads/investor/photo/1/cropped4991818370070749122.jpg',
          portfolio_value: 350000.0,
          first_access: false,
          super_angel: false
        },
      ]);
    });
};
