const jwt = require('jsonwebtoken');
require('dotenv').config();

const errorMessage = (res) => {
    return res.status(401).json({
        errors: [
            "You need to sign in or sign up before continuing."
        ]
    }); 
}

module.exports = {
    isAuthenticated: (req, res, next) => {
        const {uid, client } = req.headers;
        const access_token = req.headers['access-token'];

        if(!uid || !access_token || !client) {
            return errorMessage(res);
        }

        jwt.verify(access_token, process.env.JWT_SECRET_DEV, (err, decoded) => {
            if (err) {
                return errorMessage(res);
            } else {
                const userId = decoded['email'];
                const cId    = decoded['clientId'];
                const clientToken = decoded['client'];
                
                if(clientToken !== client || userId !== uid) {
                    return errorMessage(res);
                }

                jwt.verify(client, process.env.JWT_SECRET_DEV, (error,tokenDecoded) => {
                    if(err) {
                        return errorMessage(res);
                    } else {
                        const { email, clientId } = tokenDecoded;
                        
                        if(email !== uid || clientId !== cId)
                            return errorMessage(res);

                        return next();
                    }
                });
            }
        });
    }
}