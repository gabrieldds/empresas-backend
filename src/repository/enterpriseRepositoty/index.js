const connection = require('../../database/connection');

module.exports = {
    find: async (filters = {}, attributes = []) => {
        return await connection('enterprises')
            .join('enterprise_types', 'enterprises.type_id', '=', 'enterprise_types.id')
            .where((qb) => {
                Object.entries(filters).forEach(([column, filterOp]) => {
                    qb.andWhere(column, filterOp.op, filterOp.filter)
                });
            })
            .select(attributes)
            .orderBy('enterprises.id');
    },

    findById: async (id, attributes) => {
        return await connection('enterprises')
            .join('enterprise_types', 'enterprises.type_id', '=', 'enterprise_types.id')
            .where('enterprises.id', id)
            .select(attributes)
            .orderBy('enterprises.id');
    }
}