const connection = require('../../database/connection');

module.exports = {
    find: async (filters = {}, attributes = []) => {
        const result = await connection('investors_enterprises')
            .join('investors', 'investors_enterprises.investor_id', '=', 'investors.id')
            .join('enterprises', 'investors_enterprises.enterprise_id', '=', 'enterprises.id')
            .where((qb) => {
                Object.entries(filters).forEach(([column, filterOp]) => {
                    qb.andWhere(column, filterOp.op, filterOp.filter)
                });
            })
            .select(attributes)
        
        const data = {
            investor: {

            },
            enterprise: undefined
        };

        result.forEach((row) => {
            if(!('id' in data.investor)) {
                data['investor'] = {
                    id: row.investor_id,
                    investor_name: row.investor_name,
                    email: row.email,
                    password: row.password,
                    city: row.city,
                    country: row.country,
                    balance: row.balance,
                    photo: row.photo,
                    portfolio: {
                        enterprises_number: result.length,
                        enterprises: []
                    },
                    portfolio_value: row.portfolio_value,
                    first_access: row.first_access,
                    super_angel: row.super_angel,
                }
                data['enterprise'] = row.enterprise;
            }

            const enterprise = {
                id: row.enterprise_id,
                enterprise_name: row.enterprise_name,
                description: row.description,
            }

            data['investor'].portfolio.enterprises.push(enterprise);
        });

        return data['investor'];
    },
}