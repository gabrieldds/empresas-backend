const investorRepository = require('../../repository/investorRepository');
const bcrypt             = require('bcryptjs');
const crypto             = require('crypto');
const jwt                = require('jsonwebtoken');
require('dotenv').config();


const failureMessage = (res) => {
    return res.status(401).send({
        "sucess": false,
        errors: [
            "Invalid login credentials. Please try again."
        ]
    });
}

module.exports = {
    signIn: async (req, res) => {

        const { email, password } = req.body;

        let access_token;
        let exp;
        let client;

        const filter = {
            email: {
                op: '=',
                filter: email
            }
        }

        const user = await investorRepository.find(filter, [
            'investors.*', 'enterprises.enterprise_name', 'enterprises.description']);

        if(!user) {
            return failureMessage(res);
        }

        const passwordIsValid = bcrypt.compareSync(password, user.password);

        if(!passwordIsValid) {
            return failureMessage(res);
        }

        //register client Id
        const clientId = crypto.randomBytes(16).toString('HEX');
        client = jwt.sign({ email, clientId }, process.env.JWT_SECRET_DEV, {
            expiresIn: '14d'
        });

        access_token = jwt.sign({ email, clientId, client }, process.env.JWT_SECRET_DEV, {
            expiresIn: '14d'
        });

        exp = jwt.decode(access_token).exp;

        res.header('access-token', access_token);
        res.header('uid', email);
        res.header('client', client);
        res.header('expiry', exp);

        delete user.password;
        user.success = true;
        return res.status(200).send({user});
    }
}