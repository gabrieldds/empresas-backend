const express = require('express');
const authController = require('./authController');

const router = express.Router();

router.post('/sign_in',
(req, res, next) => {
    authController.signIn(req, res).catch(next);
});

module.exports = router;