const enterpriseRepository = require('../../repository/enterpriseRepositoty');

const mapJoin = (data) => {
    const { type_id, enterprise_type_name, ...enterprise} = data;
    enterprise.enterprise_type = {
        id: type_id,
        enterprise_type_name,
    }
    return { 
        enterprise,
        success: true
    }
}

const isEmpty = (array) => {
    return array.length === 0 || typeof array === 'undefined'
}

module.exports = {
    find: async(req, res) => {
        const { enterprise_types, name } = req.query;

        const attributes = ['enterprises.*', 'enterprise_types.enterprise_type_name',];
        let filters = {};
        
        if(enterprise_types) {
            filters.type_id = {
                op: '=',
                filter: enterprise_types,
            }
        }

        if(name) {
            filters.enterprise_name = {
                op: 'ilike',
                filter: `${name}%`,
            }
        }

        const enterprises = await enterpriseRepository.find(filters, attributes);

        if(isEmpty(enterprises)) {
            return res.json({enterprises});
        }

        const result = enterprises.map((enterprise) => mapJoin(enterprise));

        return res.status(200).json(result);
    },

    findById: async(req, res) => {
        const { id } = req.params;

        const attributes = ['enterprises.*', 'enterprise_types.enterprise_type_name'];
        const enterprises = await enterpriseRepository.findById(id, attributes);

        if(isEmpty(enterprises)) {
            return res.status(404).json({
                status: '404',
                error: 'Not Found'
            });
        }

        const [result] = enterprises.map((enterprise) => mapJoin(enterprise));

        return res.json(result);
    },
}