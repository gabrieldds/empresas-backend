const express = require('express');
const enterpriseController = require('./enterprisesController');
const authenticated = require('../../middleware/authenticated');

const router = express.Router();

router.get('/', authenticated.isAuthenticated, 
(req, res, next) => {
    enterpriseController.find(req, res).catch(next);
});

router.get('/:id', authenticated.isAuthenticated, 
(req, res, next) => {
    enterpriseController.findById(req, res).catch(next);
});

module.exports = router;