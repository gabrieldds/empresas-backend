const express     = require('express');
const enterprises = require('./enterprises');
const auth        = require('./auth');

const router = express.Router();

router.use('/enterprises', enterprises);
router.use('/users/auth', auth);

module.exports = router;