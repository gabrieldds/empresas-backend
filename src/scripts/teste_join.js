const investorRepository = require('../repository/investorRepository');

const filter = {
    email: {
        op: '=',
        filter: 'testeapple@ioasys.com.br'
    }
}

investorRepository.find(filter, ['*']).then((investors) => {
    console.log(investors);
});